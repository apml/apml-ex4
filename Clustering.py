import numpy as np
import matplotlib.pyplot as plt
import pickle

import scipy.spatial.distance as sp_dist
import scipy.linalg as sp_linalg
from matplotlib import cm

from sklearn.datasets import load_digits
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA


def synthetic_circles(k):
    """
    an example function for generating and plotting synthetic data.
    """

    t = np.arange(0, 2 * np.pi, 0.03)
    length = np.shape(t)
    length = length[0]
    circles_l = []
    for r in range(k):
        circle = np.matrix([r * np.cos(t) + 0.1 * np.random.randn(length),
                            r * np.sin(t) + 0.1 * np.random.randn(length)])
        circles_l.append(circle)

    circles = np.concatenate(circles_l, axis=1)
    real_cluster = np.repeat(np.arange(k), length)

    return circles.T, real_cluster


def apml_pic_example(path='APML_pic.pickle'):
    """
    an example function for loading and plotting the APML_pic data.

    :param path: the path to the APML_pic.pickle file
    """

    with open(path, 'rb') as f:
        apml = pickle.load(f)

    plt.plot(apml[:, 0], apml[:, 1], '.')
    plt.show()


def microarray_exploration(data_path='microarray_data.pickle',
                           genes_path='microarray_genes.pickle',
                           conds_path='microarray_conds.pickle'):
    """
    an example function for loading and plotting the microarray data.
    Specific explanations of the data set are written in comments inside the
    function.

    :param data_path: path to the data file.
    :param genes_path: path to the genes file.
    :param conds_path: path to the conds file.
    """

    # loading the data:
    with open(data_path, 'rb') as f:
        data = pickle.load(f)
    with open(genes_path, 'rb') as f:
        genes = pickle.load(f)
    with open(conds_path, 'rb') as f:
        conds = pickle.load(f)

    # look at a single value of the data matrix:
    i = 128
    j = 63
    print("gene: " + str(genes[i]) + ", cond: " + str(
        conds[0, j]) + ", value: " + str(data[i, j]))

    # make a single bar plot:
    plt.figure()
    plt.bar(np.arange(len(data[i, :])) + 1, data[i, :])
    plt.show()

    # look at two conditions and their correlation:
    plt.figure()
    plt.scatter(data[:, 27], data[:, 29])
    plt.plot([-5, 5], [-5, 5], 'r')
    plt.show()

    # see correlations between conds:
    correlation = np.corrcoef(np.transpose(data))
    plt.figure()
    plt.imshow(correlation, extent=[0, 1, 0, 1])
    plt.colorbar()
    plt.show()

    # look at the entire data set:
    plt.figure()
    plt.imshow(data, extent=[0, 1, 0, 1], cmap="hot", vmin=-3, vmax=3)
    plt.colorbar()
    plt.show()


def euclid(X, Y):
    """
    return the pair-wise euclidean distance between two data matrices.
    :param X: NxD matrix.
    :param Y: MxD matrix.
    :return: NxM euclidean distance matrix.
    """

    return sp_dist.cdist(X, Y, metric='euclidean')


def euclidean_centroid(X):
    """
    return the center of mass of data points of X.
    :param X: a sub-matrix of the NxD data matrix that defines a cluster.
    :return: the centroid of the cluster.
    """

    # TODO: YOUR CODE HERE
    return np.mean(X, axis=0)


def kmeans_pp_init(X, k, metric):
    """
    The initialization function of kmeans++, returning k centroids.
    :param X: The data matrix.
    :param k: The number of clusters.
    :param metric: a metric function like specified in the kmeans documentation.
    :return: kxD matrix with rows containing the centroids.
    """

    # TODO: YOUR CODE HERE
    N, D = X.shape
    centroids = np.zeros((k, D))

    # first in random
    centroids[0] = X[np.random.choice(X.shape[0], 1, replace=False), :]
    dists = np.full((N, k), np.inf)

    for i in range(1, k):
        dists[:, i - 1] = metric(X, centroids[i - 1, np.newaxis]).ravel()
        d = np.min(dists, axis=1)
        w = d ** 2
        w /= np.sum(w)
        centroids[i] = X[np.random.choice(X.shape[0], 1, replace=False, p=w), :]

    return centroids


def kmeans(X, k, iterations=10, metric=euclid, center=euclidean_centroid, init=kmeans_pp_init):
    """
    The K-Means function, clustering the data X into k clusters.
    :param X: A NxD data matrix.
    :param k: The number of desired clusters.
    :param iterations: The number of iterations.
    :param metric: A function that accepts two data matrices and returns their
            pair-wise distance. For a NxD and KxD matrices for instance, return
            a NxK distance matrix.
    :param center: A function that accepts a sub-matrix of X where the rows are
            points in a cluster, and returns the cluster centroid.
    :param init: A function that accepts a data matrix and k, and returns k initial centroids.
    :param stat: A function for calculating the statistics we want to extract about
                the result (for K selection, for example).
    :return: a tuple of (clustering, centroids, statistics)
    clustering - A N-dimensional vector with indices from 0 to k-1, defining the clusters.
    centroids - The kxD centroid matrix.
    """
    N, _ = X.shape

    centroids = init(X, k, metric)
    clustering = np.zeros(N)

    for _ in range(iterations):
        dists = metric(X, centroids)
        min_d = np.argmin(dists, axis=1)
        clustering = min_d
        for i in range(k):
            x = center(X[min_d == i])
            centroids[i] = x

    return clustering, centroids


def gaussian_kernel(distances, sigma):
    """
    calculate the gaussian kernel similarity of the given distance matrix.
    :param distances: A NxN distance matrix.
    :param sigma: The width of the Gaussian in the heat kernel.
    :return: NxN similarity matrix.
    """
    w = np.exp(- (distances ** 2) / (2 * sigma ** 2))
    return w


def mnn(distances, m):
    """
    calculate the m nearest neighbors similarity of the given distance matrix.
    :param distances: A NxN distance matrix.
    :param m: The number of nearest neighbors.
    :return: NxN similarity matrix.
    """
    N, _ = distances.shape
    w = np.zeros(distances.shape)

    m_near = np.argpartition(distances, m, axis=1)[:, :m]
    np.put_along_axis(w, m_near, 1, axis=1)

    return w


def spectral(data, k, similarity_param, similarity=gaussian_kernel):
    """
    Cluster the data into k clusters using the spectral clustering algorithm.
    :param eig_vecs: A NxD data matrix.
    :param k: The number of desired clusters.
    :param similarity_param: m for mnn, sigma for the Gaussian kernel.
    :param similarity: The similarity transformation of the data.
    :return: clustering, as in the kmeans implementation.
    """
    N, D = data.shape
    distances = sp_dist.cdist(data, data, metric='euclidean')
    W = similarity(distances, similarity_param)
    D_sqrt = np.zeros(W.shape)
    D_sqrt.flat[::N + 1] = np.power(np.sum(W, axis=0), -0.5)
    L = np.identity(N) - D_sqrt.dot(W).dot(D_sqrt)
    # L is almost symmetric (the difference between coordinates < 1e-10),
    # so I made it symmetric so I can use eigh which expect real symmetric matrix
    L = (L + L.T) / 2

    # get k smallest
    eig_vals, eig_vecs = sp_linalg.eigh(L, eigvals=(0, k - 1))

    # discard imaginary part
    eig_vecs = eig_vecs.real
    # normalize
    rows_norm = np.linalg.norm(eig_vecs, axis=1, ord=2)
    to_cluster = eig_vecs / rows_norm[:, np.newaxis]
    # caluclate kmeans
    clustering, centroids = kmeans(to_cluster, k)

    return clustering, centroids


def sse(data, clusters, k):
    s = 0
    for i in range(k):
        in_cluster = data[np.argwhere(clusters == i).ravel()]
        s += np.sum(sp_dist.pdist(in_cluster, metric='sqeuclidean'))
    return s


###############################################
#        from here it is testing code         #
def run_kmeans_circles(real_k):
    data, real = synthetic_circles(real_k)

    k = 1

    plt.figure()
    columns = 3
    rows = 3
    fig, ax_array = plt.subplots(rows, columns, squeeze=False, figsize=(20, 20))
    for ax_row in ax_array:
        for axes in ax_row:
            axes.set_title(f'k={k}', fontsize=25)
            axes.set_yticklabels([])
            axes.set_xticklabels([])

            clustering, centroids = kmeans(data, k)
            axes.scatter(np.ravel(data[:, 0]), np.ravel(data[:, 1]), c=clustering)
            for x, y in centroids:
                axes.scatter(x, y, s=50, c='red', marker='+')

            k += 1

    fig.savefig('out/circles_kmeans.png')


def run_spectral_circles(real_k):
    data, real = synthetic_circles(real_k)

    costs = []
    k = 1
    sig = 0.1  # chosen after some testing

    plt.figure()
    columns = 3
    rows = 3
    fig, ax_array = plt.subplots(rows, columns, squeeze=False, figsize=(20, 20))
    for ax_row in ax_array:
        for axes in ax_row:
            axes.set_title(f'k={k}', fontsize=25)
            axes.set_yticklabels([])
            axes.set_xticklabels([])

            clustering, centroids = spectral(data, k, sig, gaussian_kernel)
            costs.append(sse(data, clustering, k))
            axes.scatter(np.ravel(data[:, 0]), np.ravel(data[:, 1]), c=clustering)

            k += 1

    fig.savefig('out/circles.png')

    fig = plt.figure()
    plt.plot(range(1, k), costs)
    fig.savefig('out/circles_cost.png')


def run_apml():
    with open('APML_pic.pickle', 'rb') as f:
        data = pickle.load(f)

    start_k = 5
    k = start_k
    # chosen after some testing
    sig = 5
    m = 10
    costs = {'kmeans': [], 'sp_g': [], 'sp_mn': []}

    plt.figure()
    columns = 3
    rows = 6
    fig, ax_array = plt.subplots(rows, columns, squeeze=False, figsize=(20, 40))
    for ax_row in ax_array:
        ax_row_it = iter(ax_row)

        # kmeans++
        axes = next(ax_row_it)
        axes.set_title(f'kmeans++\nk={k}', fontsize=25)
        axes.set_yticklabels([])
        axes.set_xticklabels([])

        clustering, centroids = kmeans(data, k)
        costs['kmeans'].append(sse(data, clustering, k))
        axes.scatter(np.ravel(data[:, 0]), np.ravel(data[:, 1]), c=clustering)
        for x, y in centroids:
            axes.scatter(x, y, s=50, c='red', marker='+')

        # spectral gaussian_kernel
        axes = next(ax_row_it)
        axes.set_title(f'spectral gaussian (sig={sig})\nk={k}', fontsize=25)
        axes.set_yticklabels([])
        axes.set_xticklabels([])

        clustering, centroids = spectral(data, k, sig, gaussian_kernel)
        costs['sp_g'].append(sse(data, clustering, k))
        axes.scatter(np.ravel(data[:, 0]), np.ravel(data[:, 1]), c=clustering)

        # spectral mnn
        axes = next(ax_row_it)
        axes.set_title(f'spectral mnn (m={m})\nk={k}', fontsize=25)
        axes.set_yticklabels([])
        axes.set_xticklabels([])

        clustering, centroids = spectral(data, k, m, mnn)
        costs['sp_mn'].append(sse(data, clustering, k))
        axes.scatter(np.ravel(data[:, 0]), np.ravel(data[:, 1]), c=clustering)

        k += 1

    fig.savefig('out/apml_kmeans_vs_spectral.png')

    plt.figure()
    plt.title(f'costs af function of k')
    fig, ax_array = plt.subplots(1, 3, squeeze=False, figsize=(30, 10))
    ax_row_it = iter(ax_array[0])

    axes = next(ax_row_it)
    axes.set_title(f'kmeans++', fontsize=25)
    axes.set_xticks(range(start_k, k))
    axes.plot(range(start_k, k), costs['kmeans'])

    axes = next(ax_row_it)
    axes.set_title(f'spectral gaussian (sig={sig})', fontsize=25)
    axes.set_xticks(range(start_k, k))
    axes.plot(range(start_k, k), costs['sp_g'])

    axes = next(ax_row_it)
    axes.set_title(f'spectral mnn (m={m})', fontsize=25)
    axes.set_xticks(range(start_k, k))
    axes.plot(range(start_k, k), costs['sp_mn'])

    fig.savefig('out/apml_kmeans_vs_spectral_cost.png')
    # plt.show()


def run_spectral_microarray_costs():
    with open('microarray_data.pickle', 'rb') as f:
        data = pickle.load(f)

    sig = 500
    k = 6

    # data = data[:int(0.3 * len(data))]

    costs = []

    clustering, centroids = spectral(data, k, sig, mnn)
    costs.append(sse(data, clustering, k))

    k = 0
    plt.figure()
    fig, ax_array = plt.subplots(3, 2, squeeze=False, figsize=(30, 30))
    # fig.subplots_adjust(wspace=0.1, hspace=0.5)
    for row_ax in ax_array:
        for axes in row_ax:
            c_data = data[np.argwhere(clustering == k).ravel()]
            axes.set_title(f'spectral cluster #{k + 1}\nof size {len(c_data)}', fontsize=25)
            axes.imshow(c_data, extent=[0, 1, 0, 1], cmap="hot", vmin=-3, vmax=3)
            k += 1
            if k == len(centroids):
                plt.savefig(f'out/microarray_spectral_mnn_clustering.png')
                return


def tsne_mnist():
    classes = 10
    digits = load_digits(classes)

    colors = cm.rainbow(np.linspace(0, 1, classes))

    data = digits['data']
    target = digits['target']

    # metric = 'euclidean'
    # reduced_data = TSNE(n_components=2, metric=metric).fit_transform(data)
    #
    # fig = plt.figure(figsize=(10,10))
    # plt.title(f't-SNE\nmetric \"{metric}\"')
    # xs = np.ravel(reduced_data[:, 0])
    # ys = np.ravel(reduced_data[:, 1])
    # plt.scatter(xs, ys, facecolors='none', edgecolors='none')
    # for x, y, t in zip(xs, ys, target):
    #     plt.text(x, y, str(t), color=colors[t], fontsize=12)
    #
    # plt.show()
    # fig.savefig('out/t-sne.png')

    fig = plt.figure()

    reduced_data = PCA(n_components=2).fit_transform(data)

    fig = plt.figure(figsize=(10, 10))
    plt.title(f'PCA"')
    xs = np.ravel(reduced_data[:, 0])
    ys = np.ravel(reduced_data[:, 1])
    plt.scatter(xs, ys, facecolors='none', edgecolors='none')
    for x, y, t in zip(xs, ys, target):
        plt.text(x, y, str(t), color=colors[t], fontsize=12)

    plt.show()
    fig.savefig('out/pca.png')


def tsne_syn():
    classes = 4
    data, target = my_synthetic_dataset(2000, classes)

    colors = cm.rainbow(np.linspace(0, 1, classes))

    # reduced_data = TSNE(n_components=2).fit_transform(data)
    reduced_data = PCA(n_components=2).fit_transform(data)

    fig = plt.figure(figsize=(10, 10))
    plt.title(f't-SNE')
    xs = np.ravel(reduced_data[:, 0])
    ys = np.ravel(reduced_data[:, 1])
    plt.scatter(xs, ys, facecolors='none', edgecolors='none')
    for x, y, t in zip(xs, ys, target):
        plt.text(x, y, str(t), color=colors[t - 1], fontsize=12)

    plt.show()
    fig.savefig('out/syn_pca.png')


def my_synthetic_dataset(n, classes):
    D = 100

    def vec(parts):
        v = np.zeros((D,))
        step = D // (parts + 1)
        r = 0
        for i in range(parts):
            mid = int(np.random.normal(r + step, 2))
            size = int(np.random.normal(D // classes // 8, D // classes // 32))
            v[mid - size:mid + size] = 1
            r += step
        return v

    data = np.zeros((n, D))
    targets = np.zeros((n,), dtype=int)

    partitions = []
    # random partition
    for _ in range(classes - 1):
        partitions.append(int(np.random.normal(n / 4, n / 16)))
    partitions.append(n - sum(partitions))

    i = 0
    for c, p_size in enumerate(partitions):
        for _ in range(p_size):
            data[i] = vec(c + 1)
            targets[i] = c + 1
            i += 1

    p = np.random.permutation(len(data))
    data = data[p]
    targets = targets[p]

    return data, targets


if __name__ == '__main__':
    pass
